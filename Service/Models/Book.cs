using Newtonsoft.Json;

namespace Service.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("husky.books")]
    public class Book
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("id")]
        public int Id { get; set; }

        [Required]
        [StringLength(45)]
        [Column("title")]
        public string Title { get; set; }

        [ForeignKey("Author"), DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Required]
        [Column("author_id")]
        public int AuthorId { get; set; }

        [JsonIgnore]
        public virtual Author Author { get; set; }
    }
}
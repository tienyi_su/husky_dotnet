﻿namespace Service.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("husky.rentals")]
    public class Rental
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("id")]
        public int Id { get; set; }

        [Required]
        [StringLength(45)]
        [Column("title")]
        public string Title { get; set; }

        [Required]
        [StringLength(45)]
        [Column("name")]
        public string Name { get; set; }

        [Column("rental_date")]
        public DateTime? RentalDate { get; set; }
    }
}

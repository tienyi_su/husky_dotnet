using System;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Service.Models;

namespace Service.Contexts
{
    public class HuskyDbContext : DbContext
    {
        public HuskyDbContext()
            : base("name=HuskyContext")
        {
            Database.SetInitializer<HuskyDbContext>(null);
        }

        // For Effort EF6 unit tests
        public HuskyDbContext(DbConnection connection) : base(connection, true)
        {
        }

        public virtual DbSet<Author> Authors { get; set; }
        public virtual DbSet<Book> Books { get; set; }
        public virtual DbSet<Rental> Rentals { get; set; }

        public override int SaveChanges()
        {
            AddTimestamps();
            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync()
        {
            AddTimestamps();
            return await base.SaveChangesAsync();
        }

        private void AddTimestamps()
        {
            var entities = ChangeTracker.Entries().Where(x => x.Entity is Rental && (x.State == EntityState.Added));

            foreach (var entity in entities)
            {
                if (entity.State == EntityState.Added)
                {
                    ((Rental) entity.Entity).RentalDate = DateTime.UtcNow;
                }
            }
        }
    }
}

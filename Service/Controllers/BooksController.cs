﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using Service.Contexts;
using Service.Models;

namespace Service.Controllers
{
    [RoutePrefix("api/books")]
    public class BooksController : ApiController
    {
        [HttpGet]
        [Route("")]
        public IList<Book> GetBooks()
        {
            using (var context = new HuskyDbContext())
            {
                return context.Books.ToList();
            }
        }

        [HttpGet]
        [Route("{id}")]
        [ResponseType(typeof(Book))]
        public IHttpActionResult GetBook(int id)
        {
            using (var context = new HuskyDbContext())
            {
                var book = context.Books.Find(id);

                if (book == null)
                {
                    return NotFound();
                }

                return Ok(book);
            }
        }
    }
}
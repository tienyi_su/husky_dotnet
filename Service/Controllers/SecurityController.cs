﻿using System.IdentityModel.Tokens.Jwt;
using System.Web;
using System.Web.Http;

namespace Service.Controllers
{
    [RoutePrefix("api/security")]
    public class SecurityController : ApiController
    {
        /// <summary>
        /// Decodes the JWT passed in the authorization header.
        /// </summary>
        /// <returns>JWT payload.</returns>
        [HttpGet]
        [Route("decode")]
        public IHttpActionResult Decode()
        {
            var httpContext = HttpContext.Current;
            var header = httpContext.Request.Headers["Authorization"].Replace("Bearer ", "");
            var jwtHandler = new JwtSecurityTokenHandler();

            // Check if readable token (string is in a JWT format)
            var isValidToken = jwtHandler.CanReadToken(header);

            if (isValidToken == false)
            {
                return BadRequest("Invalid token!");
            }
            
            var token = jwtHandler.ReadJwtToken(header);

            return Ok(token.Payload);
        }
    }
}
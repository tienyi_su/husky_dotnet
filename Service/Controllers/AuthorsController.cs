﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using Service.Contexts;
using Service.Models;

namespace Service.Controllers
{
    [RoutePrefix("api/authors")]
    public class AuthorsController : ApiController
    {
        [HttpGet]
        [Route("")]
        public IList<Author> GetAuthors()
        {
            using (var context = new HuskyDbContext())
            {
                return context.Authors.ToList();
            }
        }

        [HttpGet]
        [Route("{id}")]
        [ResponseType(typeof(Author))]
        public IHttpActionResult GetAuthor(int id)
        {
            using (var context = new HuskyDbContext())
            {
                var author = context.Authors.Find(id);

                if (author == null)
                {
                    return NotFound();
                }

                return Ok(author);
            }
        }
    }
}
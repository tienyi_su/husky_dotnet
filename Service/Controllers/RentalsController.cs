﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using Service.Contexts;
using Service.DTOs;
using Service.Models;
using Service.Repositories;

namespace Service.Controllers
{
    [RoutePrefix("api/rentals")]
    public class RentalsController : ApiController
    {
        private readonly RentalsRepository _repository;

        public RentalsController()
        {
            _repository = new RentalsRepository();
        }

        [HttpGet]
        [Route("")]
        public IList<Rental> GetRentals()
        {
            using (var context = new HuskyDbContext())
            {
                return context.Rentals.ToList();
            }  
        }

        [HttpGet]
        [Route("{id}")]
        [ResponseType(typeof(Rental))]
        public IHttpActionResult GetRental(int id)
        {
            using (var context = new HuskyDbContext())
            {
                var rental = context.Rentals.Find(id);

                if (rental == null)
                {
                    return NotFound();
                }

                return Ok(rental);
            }
        }

        [HttpGet]
        [Route("authors")]
        [ResponseType(typeof(AuthorDto))]
        public IList<AuthorDto> GetRentalsWithAuthors()
        {
            return _repository.GetRentalAuthors();
        }
    }
}
﻿namespace Service.DTOs
{
    public class AuthorDto
    {
        public string FirstName
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using Service.Contexts;
using Service.DTOs;

namespace Service.Repositories
{
    public class RentalsRepository
    {
        private readonly HuskyDbContext _dbContext;

        public RentalsRepository()
        {
            _dbContext = new HuskyDbContext();
        }

        public RentalsRepository(HuskyDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Gets a list of rented books with their authors.
        /// </summary>
        /// <returns>List of AuthorDto objects.</returns>
        public IList<AuthorDto> GetRentalAuthors()
        {
            return (from author in _dbContext.Authors
                join book in _dbContext.Books on author.Id equals book.AuthorId
                join rental in _dbContext.Rentals on book.Title equals rental.Title
                select new AuthorDto
                {
                    FirstName = author.FirstName,
                    LastName = author.LastName
                }).ToList();
        }

        /*        /// <summary>
                /// Gets a list of rented books with their authors.
                /// </summary>
                /// <returns>List of AuthorDto objects.</returns>
                public IList<AuthorDto> GetRentalAuthors()
                {
                    return context.Authors
                        .Join(context.Books,
                            author => author.Id,
                            book => book.AuthorId,
                            (author, book) => new {author, book}
                        )
                        .Join(context.Rentals,
                            rented => rented.book.Title,
                            rental => rental.Title,
                            (rented, rental) => new {rented, rental}
                        )
                        .Select(result => new AuthorDTO
                        {
                            FirstName = result.rented.author.FirstName,
                            LastName = result.rented.author.LastName
                        }).ToList();
                }*/

        /*        /// <summary>
                /// Gets a list of rented books with their authors.
                /// </summary>
                /// <returns>List of AuthorDto objects.</returns>
                public IList<AuthorDto> GetRentalAuthors()
                {
                    return context.Database
                        .SqlQuery<AuthorDTO>("select ha.first_name, ha.last_name from husky.authors ha" +
                                             " inner join husky.books hb on ha.id = hb.author_id inner join husky.rentals hr on hr.title = hb.title")
                        .ToList();
                }*/
    }
}
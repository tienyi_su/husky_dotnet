﻿using Effort;
using Service.Contexts;
using NUnit.Framework;
using Service.Models;
using Service.Repositories;
using Assert = NUnit.Framework.Assert;

namespace Service.Test.Repositories
{
    [TestFixture]
    public class RentalsRepositoryTests
    {
        private HuskyDbContext _dbContext;
        private RentalsRepository _repository;

        [SetUp]
        public void SetUp()
        {
            var connection = DbConnectionFactory.CreateTransient();
            _dbContext = new HuskyDbContext(connection);

            PrepareData();

            _repository = new RentalsRepository(_dbContext);
        }

        [Test]
        public void GetRentalAuthors_validQuery_returnAuthorList()
        {
            var result = _repository.GetRentalAuthors();

            Assert.AreEqual(2, result.Count);
            Assert.AreEqual("George", result[0].FirstName);
            Assert.AreEqual("Orwell", result[0].LastName);
            Assert.AreEqual("Arthur Conan", result[1].FirstName);
            Assert.AreEqual("Doyle", result[1].LastName);
        }

        private void PrepareData()
        {
            _dbContext.Database.CreateIfNotExists();

            _dbContext.Authors.Add(new Author { Id = 1, FirstName = "H.G", LastName = "Wells"});
            _dbContext.Authors.Add(new Author { Id = 2, FirstName = "George", LastName = "Orwell" });
            _dbContext.Authors.Add(new Author { Id = 3, FirstName = "Arthur Conan", LastName = "Doyle" });
            _dbContext.Authors.Add(new Author { Id = 4, FirstName = "Isaac", LastName = "Asimov" });

            _dbContext.Books.Add(new Book { Id = 1, Title = "A Study in Scarlet", AuthorId = 3});
            _dbContext.Books.Add(new Book { Id = 2, Title = "I, Robot", AuthorId = 4 });
            _dbContext.Books.Add(new Book { Id = 3, Title = "The Time Machine", AuthorId = 1 });
            _dbContext.Books.Add(new Book { Id = 4, Title = "The Hound of the Baskervilles", AuthorId = 3 });
            _dbContext.Books.Add(new Book { Id = 5, Title = "The War of the Worlds", AuthorId = 1 });
            _dbContext.Books.Add(new Book { Id = 6, Title = "The Sign of Four", AuthorId = 3 });
            _dbContext.Books.Add(new Book { Id = 7, Title = "1984", AuthorId = 2 });
            _dbContext.Books.Add(new Book { Id = 8, Title = "Animal Farm", AuthorId = 2 });

            _dbContext.Rentals.Add(new Rental { Id = 1, Title = "1984", Name = "John Doe" });
            _dbContext.Rentals.Add(new Rental { Id = 2, Title = "The Hound of the Baskervilles", Name = "Clark Kent" });

            _dbContext.SaveChanges();
        }
    }
}

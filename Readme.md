Simple .NET pet project to PoC Entity Framework 6 integration with existing model class and MySQL database.

### Dependencies
* .NET Framework 4.5
* Entity Framework 6
* Effort.EF6
* MySQL for Visual Studio
* MySQL Connector for .NET

### Current Features
#### Entity Framework 6 integration
* Integration of EF6 with an existing MySQL database schema and model classes using DataAnnotations.
* Service class method to explore 3 different ways to query using EF6 - LINQ query syntax, LINQ method syntax, raw SQL query.
* Overridden method in DbContext class to track changes to model and auto-populate date field.
* Unit test using existing DbContext (not mocked) using Effort.EF6.
